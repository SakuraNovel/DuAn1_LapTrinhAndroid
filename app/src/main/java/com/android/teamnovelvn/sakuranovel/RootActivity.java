package com.android.teamnovelvn.sakuranovel;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;

import java.util.Random;

import static com.android.teamnovelvn.sakuranovel.R.id.boom;


public class RootActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout = null;

    private boolean init = false;
    private BoomMenuButton boomMenuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(getResources().getString(R.string.app_name));
        toolbarTextAppernce();
//        dynamicToolbarColor();

        initViews();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);

        if (init) return;
        init = true;

        initBoom();
    }

    private void initBoom(){
        int number = 9;
        Drawable[] drawables = new Drawable[number];
        int[] drawablesResource = new int[]{
                R.drawable.home,
                R.drawable.login,
                R.drawable.copy,
                R.drawable.heart,
                R.drawable.search,
                R.drawable.settings,
                R.drawable.faq,
                R.drawable.feedback,
                R.drawable.info

        };
        for (int i = 0; i < number ; i++){
            drawables[i]  = ContextCompat.getDrawable(this,drawablesResource[i]);
        }
        String[] STRINGS = new String[]{
                "Home",
                "Login",
                "Type",
                "My Favorites",
                "Search",
                "Setting",
                "FAQ",
                "Feedback",
                "Info"
        };
        String[] strings = new String[number];
        for (int i = 0; i < number ; i++){
            strings[i]  = STRINGS[i];
        }
        int[][] colors = new int[number][2];
        for (int i = 0 ;i < number; i++){
            colors[i][1] = getRandomColor();
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }

        new BoomMenuButton.Builder()
                .subButtons(drawables,colors,strings)
                .button(ButtonType.CIRCLE)
                .boom(BoomType.HORIZONTAL_THROW)
                .place(getPlaceType())
                .boomButtonShadow(Util.getInstance().dp2px(2),Util.getInstance().dp2px(2))
                .subButtonsShadow(Util.getInstance().dp2px(2),Util.getInstance().dp2px(2))
                .shareStyle(3f,getRandomColor(),getRandomColor())
                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
                    @Override
                    public void onClick(int buttonIndex) {
                        if (buttonIndex == 0){
                            Toast.makeText(RootActivity.this,"Home",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 1){
                            Toast.makeText(RootActivity.this,"Login",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 2){
                            Toast.makeText(RootActivity.this,"Type",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 3){
                            Toast.makeText(RootActivity.this,"My Favorites",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 4){
                            Toast.makeText(RootActivity.this,"Search",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 5){
                            Toast.makeText(RootActivity.this,"Setting",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 6){
                            Toast.makeText(RootActivity.this,"FAQ",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 7){
                            Toast.makeText(RootActivity.this,"Feedback",Toast.LENGTH_LONG).show();
                        }else if (buttonIndex == 8){
                            Toast.makeText(RootActivity.this,"Info",Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .init(boomMenuButton);
    }


    private void initViews(){
        boomMenuButton = (BoomMenuButton)findViewById(boom);
        //boomMenuButton.setShareLineWidth(100* 6f/100);
        boomMenuButton.setDuration(300);
        boomMenuButton.setRotateDegree(3240);
        getPlaceType();
        initBoom();
    }

    private PlaceType getPlaceType(){
        return PlaceType.CIRCLE_9_1;
    }

    private String[] Colors = {
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"
    };

    public int getRandomColor(){
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }

//    private void dynamicToolbarColor() {
//
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
//                R.drawable.lighting);
//        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//            @Override
//            public void onGenerated(Palette palette) {
//                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(R.attr.colorPrimary));
//                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.attr.colorPrimaryDark));
//            }
//        });
//    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }
}
